public class Main {
    /*create 3 threads where all threads perform concate operation where thread1 concates String i.e "Hii" and "hello"
     thread 2 concates "hii" and "friends " and thread 3 concates "good" and "morning"
     */
    public static void main(String[] args) {
        Concatication concatication1 = new Concatication("Hii", "hello", "thread 1");
        concatication1.setPriority(10);
        Concatication concatication2 = new Concatication("hii", "friends", "thread 2");
        concatication2.setPriority(9);
        Concatication concatication3 = new Concatication("good", "morning", "thread 3");
        concatication3.setPriority(8);
    }
}
